package awais.instagrabber.repositories.responses.discover

import awais.instagrabber.repositories.responses.WrappedMedia

data class ExploreFeed(
    val layoutContent: FeedContent?,
    val feedType:String
)
