package awais.instagrabber.repositories.responses.discover

import awais.instagrabber.repositories.responses.WrappedMedia

data class FeedContent(
    val medias: List<WrappedMedia>?
)
